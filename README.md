Use your mouse to click on the canvas, to try to move the one panel towards the other. As soon as you reach your target, a new one will spawn. The first one is really easy, but once your panel has some momentum, it becomes harder to control. You get points for every panel you reach, and lose points for every click you use.

This is a small experiment in the cursed art of golfing GDScript. Made with just 500 characters of code. Less (code) is more as they say. It was mostly just for the technical challenge, but it ended up being surprisingly fun for me, so I decided to submit it.

Play on [Itch.io](https://kinami.itch.io/panel-curling)

Made with ❤️  by [Kinami Imai 今井きなみ](https://ikinami.gitlab.io/kinami/)
